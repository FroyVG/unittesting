# UnitTesting

Demo project for Spring Boot using unit testing

##Using Okta
In order to test, you will need to be able to generate a valid token. 
Typically, the client application would be responsible for generating the tokens that it would use for authentication in the API. However, since you have no client application, you need a way to generate tokens in order to test the application.

An easy way to achieve a token is to generate one using OpenID [OpenID Connect Debugger](https://oidcdebugger.com/). First, however, you must have a client Web application setup in Okta to use with OpenID Connect’s implicit flow.

To do this, go back to the Okta developer console and select **Applications** > **Add Application**, but this time, select the Web tile.

On the next screen, you will need to fill out some information. Set the name to something you will remember as your web application. Set the **Login redirect URIs** field to https://oidcdebugger.com/debug and **Grant Type Allowed** to **Hybrid**. Click **Done** and copy the client ID for the next step.

##Send requests
curl -X POST http://localhost:8080/birthday/{hereEditYourPath} -H "Authorization: Bearer {HereYourTokenCode}" -H "Content-Type: text/plain" -H "accept: text/plain" -d 2005-03-09
