package info.froylanvillaverde.UnitTesting.controller;

import info.froylanvillaverde.UnitTesting.service.IBirthdayService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/birthday")
public class BirthdayInfoController {

    private final IBirthdayService birthdayService;

    public BirthdayInfoController(IBirthdayService birthdayService) {
        this.birthdayService = birthdayService;
    }

    @PostMapping("/dayOfWeek")
    public String getDayOfWeek(@RequestBody String birthdayString){
        LocalDate birthDay = birthdayService.getValidBirthday(birthdayString);
        String dow = birthdayService.getBirthDOW(birthDay);
        return dow;
    }

    @PostMapping("/chineseZodiac")
    public String getChineseZodiac(@RequestBody String birthdayString){
        LocalDate birthDay = birthdayService.getValidBirthday(birthdayString);
        String sign = birthdayService.getChineseZodiac(birthDay);
        return sign;
    }

    @PostMapping("/starSign")
    public String getStarSign(@RequestBody String birthdayString){
        LocalDate birthDay = birthdayService.getValidBirthday(birthdayString);
        String sign = birthdayService.getStarSign(birthDay);
        return sign;
    }

    @ExceptionHandler(RuntimeException.class)//Tells it to catch any instance of RuntimeException within the endpoint functions and return a 500 response
    public final ResponseEntity<Exception> handleAllExceptions(RuntimeException ex){
        return new ResponseEntity<Exception>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
