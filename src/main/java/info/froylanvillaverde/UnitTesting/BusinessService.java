package info.froylanvillaverde.UnitTesting;

import info.froylanvillaverde.UnitTesting.mockito.DataService;
import org.springframework.stereotype.Service;

@Service
public class BusinessService {

    private DataService dataService;

    /**
     * Providing a constructor for injecting the data service.
     *
     * @param dataService
     */
    public BusinessService(DataService dataService) {
        super();
        this.dataService = dataService;
    }

    public int findTheGreatestFromAllData(){
        int[] data = dataService.retrieveAllData();
        int greatest = Integer.MIN_VALUE;

        for(int value :  data){
            if (value > greatest){
                greatest = value;
            }
        }
        return greatest;
    }

}
