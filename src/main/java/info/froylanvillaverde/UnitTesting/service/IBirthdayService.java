package info.froylanvillaverde.UnitTesting.service;

import java.time.LocalDate;

public interface IBirthdayService {

    LocalDate getValidBirthday(String birthdayString);

    String getBirthDOW(LocalDate birthday);

    String getChineseZodiac(LocalDate birthday);

    String getStarSign(LocalDate birthday);
}
