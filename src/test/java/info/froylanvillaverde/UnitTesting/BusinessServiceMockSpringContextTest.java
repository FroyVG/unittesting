package info.froylanvillaverde.UnitTesting;

import info.froylanvillaverde.UnitTesting.mockito.DataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)//SpringRunner is used to launch up a spring context in unit tests.
@SpringBootTest//This annotation indicates that the context under test is a SpringBootApplication
public class BusinessServiceMockSpringContextTest {

    @MockBean//Creates a Mock for DataService. This mock is used in the Spring context instead of the real DataService.
    private DataService dataServiceMock;

    @Autowired//Pick the business Service from the Spring context autowire it in.
    BusinessService businessService;

    @Test
    public void testFindTheGreatestFromAllData(){
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{24, 15, 3});
        assertEquals(24, businessService.findTheGreatestFromAllData());
    }

    @Test
    public void testFindTheGreatestFromAllData_ForOneValue(){
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{15});
        assertEquals(15, businessService.findTheGreatestFromAllData());
    }

    @Test
    public void testFindThreGreatestFromAllData_ForOneValue(){
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{});
        assertEquals(Integer.MIN_VALUE, businessService.findTheGreatestFromAllData());
    }
}
