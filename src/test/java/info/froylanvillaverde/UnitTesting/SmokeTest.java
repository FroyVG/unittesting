package info.froylanvillaverde.UnitTesting;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest//Tells Spring Boot to look for a main configuration class and use that to start a Spring application context.
public class SmokeTest {

    @Autowired
    private HomeController controller;

    @Test
    @Disabled//Disabling unit testing
    public void contextLoads() throws Exception{
        assertThat(controller).isNotNull();
    }
}
