package info.froylanvillaverde.UnitTesting;

import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

public class MyMathTest {

    MyMath myMath = new MyMath();

    @Test
    public void sum_with3numbers(){
        System.out.println("Test1");
        assertEquals(6,myMath.sum(new int[]{1,2,3}));
    }

    @Test
    public void sum_with1number(){
        System.out.println("Test2");
        assertEquals(3, myMath.sum(new int[]{3}));
    }

    @Before
    public void before(){
        System.out.println("Before");
    }

    @After
    public void after(){
        System.out.println("After");
    }

    @BeforeClass
    public static void beforeClass(){
        System.out.println("beforeClass");
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("afterClass");
    }
}
