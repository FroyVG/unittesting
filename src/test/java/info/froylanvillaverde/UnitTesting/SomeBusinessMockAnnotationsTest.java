package info.froylanvillaverde.UnitTesting;

import info.froylanvillaverde.UnitTesting.mockito.DataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)//The JUnit Runner which causes all the initialization magic
                                    // with @Mock and @InjectMocks to happen before the tests are run.
public class SomeBusinessMockAnnotationsTest {

    @Mock//Create a Mock for DataService.
    DataService dataServiceMock;

    @InjectMocks//Inject the mocks as depedencies into businessImpl.
    BusinessService businessService;

    @Test
    public void testFindTheGreatestFromAllData(){
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{24, 15, 3});
        assertEquals(24, businessService.findTheGreatestFromAllData());
    }

    @Test
    public void testFindTheGreatestFromAllData_ForOneValue(){
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{15});
        assertEquals(15, businessService.findTheGreatestFromAllData());
    }

    @Test
    public void testFindTheGreatestFromAllData_NoValues(){
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{});
        assertEquals(Integer.MIN_VALUE, businessService.findTheGreatestFromAllData());
    }

}
