package info.froylanvillaverde.UnitTesting;

import info.froylanvillaverde.UnitTesting.mockito.DataService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SomeBusinessMockTest {

    @Test
    public void testFindTheTheGreatestFromAllData() {
        DataService dataServiceMock = mock(DataService.class);//mock() method it´s used to create a mock.
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{24, 15, 3});
        BusinessService businessService = new BusinessService(dataServiceMock);
        int result = businessService.findTheGreatestFromAllData();
        assertEquals(24, result);
    }

    @Test
    public void testFindTheGreatestFromAllData_ForOneValue() {
        DataService dataServiceMock = mock(DataService.class);
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{15});
        BusinessService businessService = new BusinessService(dataServiceMock);
        int result = businessService.findTheGreatestFromAllData();
        assertEquals(15, result);
    }



}
